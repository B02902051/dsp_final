import preproVocab
import os
rootPath = '..'
dataPath = os.path.join(rootPath, 'data')
	
def main(invertedFile, IdxToDocFile, DocToIdxFile, TermNumPerDocFile, termDocSizeFile):
	TermNumPerDoc = preproVocab.readPickle(TermNumPerDocFile)
	termDocSize = preproVocab.readPickle(termDocSizeFile)
	# wordToIdxVocab = preproVocab.readPickle(wordToIdxVocabFile)
	term_size = termDocSize[0]
	corpus_inverted = {idx : {} for idx in range(term_size)}
	IdxToDoc = {}
	DocToIdx = {}
	for doc_idx, doc in enumerate(TermNumPerDoc):
		IdxToDoc[doc_idx] = doc
		DocToIdx[doc] = IdxToDoc
		for term_idx in TermNumPerDoc[doc]:
			corpus_inverted[term_idx][doc_idx] = TermNumPerDoc[doc][term_idx]
	preproVocab.writePickle(IdxToDoc, IdxToDocFile)
	preproVocab.writePickle(DocToIdx, DocToIdxFile)
	preproVocab.writePickle(corpus_inverted, invertedFile)
	return
if __name__ == '__main__':
	invertedFile = os.path.join(dataPath, 'PTV_char_inverted.pkl')
	IdxToDocFile = os.path.join(dataPath, 'IdxToDoc.pkl')
	DocToIdxFile = os.path.join(dataPath, 'DocToIdx.pkl') 
	TermNumPerDocFile = os.path.join(dataPath, 'PTV_char_TermNumPerDoc.pkl')
	termDocSizeFile = os.path.join(dataPath, args.dataType + '_' + args.entryType + '_termDocSize.pkl')

	main(invertedFile, IdxToDocFile, DocToIdxFile, TermNumPerDocFile, termDocSizeFile)
