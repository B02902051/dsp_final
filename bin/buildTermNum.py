import preproVocab
import jieba
import os

rootPath = '..'
dataPath = os.path.join(rootPath, 'data')

def build_mono(corpus_dict, wordToIdxVocab):
	termNumPerDoc = {}
	CNS_ZhuYin = preproVocab.buildMapping()
	for doc in corpus_dict:
		doc_inverted = {}
		text = preproVocab.removeSymbols(corpus_dict[doc][1])
		for word in text:
			if word in CNS_ZhuYin:
				for ZhuYin in CNS_ZhuYin[word]:
					if ZhuYin in wordToIdxVocab:
						if wordToIdxVocab[ZhuYin] in doc_inverted:
							doc_inverted[wordToIdxVocab[ZhuYin]] += 1
						else:
							doc_inverted[wordToIdxVocab[ZhuYin]] = 1
		termNumPerDoc[doc] = doc_inverted
	return termNumPerDoc
def build_bi(corpus_dict, wordToIdxVocab):
	termNumPerDoc = {}
	CNS_ZhuYin = preproVocab.buildMapping()
	for doc in corpus_dict:
		doc_inverted = {}
		text = preproVocab.removeSymbols(corpus_dict[doc][1])
		for i in range(len(text) - 1):
			if text[i] in CNS_ZhuYin and text[i + 1] in CNS_ZhuYin:
				for z1 in CNS_ZhuYin[text[i]]:
					for z2 in CNS_ZhuYin[text[i + 1]]:
						entry = z1 + z2
						if entry in wordToIdxVocab:
							if wordToIdxVocab[entry] in doc_inverted:
								doc_inverted[wordToIdxVocab[entry]] += 1
							else:
								doc_inverted[wordToIdxVocab[entry]] = 1
		termNumPerDoc[doc] = doc_inverted
	return termNumPerDoc

def build_char(corpus_dict, wordToIdxVocab):	
	termNumPerDoc = {}
	for doc in corpus_dict:
		doc_inverted = {}
		text = preproVocab.removeSymbols(corpus_dict[doc][1])
		for word in text:
			if word in wordToIdxVocab:
				if wordToIdxVocab[word] in doc_inverted:
					doc_inverted[wordToIdxVocab[word]] += 1
				else:
					doc_inverted[wordToIdxVocab[word]] = 1
		termNumPerDoc[doc] = doc_inverted
	return termNumPerDoc
def build_bichar(corpus_dict, wordToIdxVocab):
	termNumPerDoc = {}
	for doc in corpus_dict:
		doc_inverted = {}
		text = preproVocab.removeSymbols(corpus_dict[doc][1])
		for i in range(len(text) - 1):
			word = text[i] + text[i + 1]
			if word in wordToIdxVocab:
				if wordToIdxVocab[word] in doc_inverted:
					doc_inverted[wordToIdxVocab[word]] += 1
				else:
					doc_inverted[wordToIdxVocab[word]] = 1
		termNumPerDoc[doc] = doc_inverted
	return termNumPerDoc
def build_word(corpus_dict, wordToIdxVocab):
	termNumPerDoc = {}
	for doc in corpus_dict:
		doc_inverted = {}
		tmp = jieba.cut(corpus_dict[doc][1])
		words = ' '.join(tmp).split()
		for word in words:
			word = preproVocab.removeSymbols(word)
			if word in wordToIdxVocab:
				if wordToIdxVocab[word] in doc_inverted:
					doc_inverted[wordToIdxVocab[word]] += 1
				else:
					doc_inverted[wordToIdxVocab[word]] = 1
		termNumPerDoc[doc] = doc_inverted
	return termNumPerDoc

def main(entryType, TermNumPerDocFile, termDocSizeFile, corpusDictFile, wordToIdxVocabFile):
	corpus_dict =  preproVocab.readPickle(corpusDictFile)
	wordToIdxVocab = preproVocab.readPickle(wordToIdxVocabFile)
	if entryType == 'mono_syllable':
		termNumPerDoc = build_mono(corpus_dict, wordToIdxVocab)
	elif entryType == 'bi_syllable':
		termNumPerDoc = build_bi(corpus_dict, wordToIdxVocab)
	elif entryType == 'char': 
		termNumPerDoc = build_char(corpus_dict, wordToIdxVocab)
	elif entryType == 'bi_char':
		termNumPerDoc = build_bichar(corpus_dict, wordToIdxVocab)
	elif entryType == 'word':
		termNumPerDoc = build_word(corpus_dict, wordToIdxVocab)
	else:
		print('wrong entryType in buildInvertedFile')
		return 
	term_doc_size = [len(wordToIdxVocab), len(corpus_dict)]	
	preproVocab.writePickle(term_doc_size, termDocSizeFile)
	preproVocab.writePickle(termNumPerDoc, TermNumPerDocFile)
if __name__ == '__main__':
	entryType = 'char'
	TermNumPerDocFile = os.path.join(dataPath, 'PTV_char_TermNumPerDoc.pkl')
	termDocSizeFile = os.path.join(dataPath, 'termDocSize.pkl')
	corpusDictFile = os.path.join(dataPath, 'PTV_corpusDict.pkl')
	wordToIdxVocabFile = os.path.join(dataPath, 'PTV_char_WordToIdxVocabDict.pkl')
	main(entryType, TermNumPerDocFile, termDocSizeFile, corpusDictFile, wordToIdxVocabFile)
