import xml.etree.ElementTree as ET
import os 

mappingFilename = 'Big5-ZhuYin.map'
rootPath = '..'
dataPath = os.path.join(rootPath, 'data')
OOVFile = os.path.join(dataPath, 'OOV.log')
symbols = [10, 12289, 12290, 65292] 

def calculateOOV(CNS):
	fileListFile = os.path.join(rootPath, 'file-list')
	with open(fileListFile) as file:
		content = file.read()
	fileList = content.splitlines()
	word_count = 0
	OOV_count = 0
	for path in fileList:
		filename = os.path.join(rootPath, path)
		tree = ET.parse(filename)
		root = tree.getroot()
		for tag in root.iter('p'):
			text = tag.text
			for word in text:
				if ord(word) in symbols:
					continue
				if word not in CNS:
					OOV_count += 1
				word_count += 1
		# print(path)
	print('word size      : ' + str(word_count))
	print('OOV size       : ' + str(OOV_count))
	print('OOV persentage : ' + str(float(OOV_count) / word_count))

	fout = open(OOVFile, 'w')
	fout.write('word size      : ' + str(word_count) + '\n')
	fout.write('OOV size       : ' + str(OOV_count) + '\n')
	fout.write('OOV persentage : ' + str(float(OOV_count) / word_count) + '\n')
	fout.close()

def buildVocab():
	filename = os.path.join(rootPath, mappingFilename)
	with open(filename) as file:
		content = file.read()
	lines = content.splitlines()
	CNS = [line.split()[0] for line in lines]
	return CNS

def main():
	CNS = buildVocab()
	calculateOOV(CNS)

if __name__ == '__main__':
	main()