import buildInverted
import buildTermNum
import preproVocab
import buildVSM
import argparse
import pickle
import os 

rootPath = '..'
corpusPath = os.path.join(rootPath, 'corpus')
dataPath = os.path.join(rootPath, 'data')
queryPath = os.path.join(rootPath, 'query')

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--foo', help='foo help')
# parser.add_argument("-i", "--dataDirectory", help="path of data containing 3 directory, train, unlabeled and test", required=True)
# parser.add_argument("-o", "--outputfile", help="output file name", required=True)
parser.add_argument('-t', "--entryType", help="the type of entry in VSM, ex: mono, bi, word, char", required=True)
parser.add_argument('-d', "--dataType", help="the type of dataset, ex: CIRB, PTV", required=True)
args = parser.parse_args()

print("args.entryType : " + args.entryType)
print("args.dataType : " + args.dataType)

corpusDictFile = os.path.join(dataPath, args.dataType + '_corpusDict.pkl')
wordToIdxVocabFile = os.path.join(dataPath,  args.dataType + '_' + args.entryType + '_WordToIdxVocabDict.pkl')
IdxToCountVocabFile = os.path.join(dataPath, args.dataType + '_' + args.entryType + '_IdxToCountVocab.pkl')
TermNumPerDocFile = os.path.join(dataPath, args.dataType + '_' + args.entryType + '_TermNumPerDoc.pkl')
invertedFile = os.path.join(dataPath, args.dataType + '_' + args.entryType + '_inverted.pkl')
termDocSizeFile = os.path.join(dataPath, args.dataType + '_' + args.entryType + '_termDocSize.pkl')
IdxToDocFile = os.path.join(dataPath, args.dataType + '_IdxToDoc.pkl')
DocToIdxFile = os.path.join(dataPath, args.dataType + '_DocToIdx.pkl')
averageDocLenFile = os.path.join(dataPath, args.dataType + '_averageDocLen.pkl')
### preprocess the corpus to build a dict of dataset
if not os.path.exists(corpusDictFile):
	print('building ' + corpusDictFile)
	if args.dataType == 'CIRB':
		preproVocab.transformFormat_CIRB(corpusPath, corpusDictFile, averageDocLenFile)
	elif args.dataType == 'PTV':
		preproVocab.transformFormat_PTV(os.path.join(corpusPath, args.dataType), corpusDictFile, averageDocLenFile)
	else:
		print('wrong dataType!')
### build wordToIdx and IdxToCount file

######## modify the path of corpus to the place where the dict is
if not os.path.exists(wordToIdxVocabFile) or not os.path.exists(IdxToCountVocabFile):
	print('building ' + wordToIdxVocabFile + ' and ' + IdxToCountVocabFile)
	preproVocab.main(corpusDictFile, args.entryType, wordToIdxVocabFile, IdxToCountVocabFile)
if not os.path.exists(TermNumPerDocFile):
	print('building ' + TermNumPerDocFile)
	buildTermNum.main(args.entryType, TermNumPerDocFile, termDocSizeFile, corpusDictFile, wordToIdxVocabFile)
if not os.path.exists(invertedFile):
	print('building ' + invertedFile)
	buildInverted.main(invertedFile, IdxToDocFile, DocToIdxFile, TermNumPerDocFile, termDocSizeFile)


print('building VSM model...')
model_matrix = buildVSM.main(args.entryType, corpusDictFile, invertedFile, termDocSizeFile, IdxToDocFile, averageDocLenFile)
print('model_matrix = ' + str(model_matrix.shape[0]) + ' * ' + str(model_matrix.shape[1]))

print('building Query model...')
query_matrix = buildVSM.buildQueryMatrix(args.entryType, args.dataType, wordToIdxVocabFile, invertedFile, termDocSizeFile)
print('query_matrix = ' + str(query_matrix.shape[0]) + ' * ' + str(query_matrix.shape[1]))
ranking = buildVSM.predict(model_matrix, query_matrix)
print(ranking)