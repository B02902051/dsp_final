from scipy.sparse import csr_matrix, lil_matrix 
import numpy as np
import preproVocab
import xml.etree.ElementTree as ET
import math
import pickle
import jieba
import os

BM_K = 0.75
BM_B = 1.4

rootPath = '..'
dataPath = os.path.join(rootPath, 'data')
queryPath = os.path.join(rootPath, 'query')

def pruneQuery_CIRB(queryFile):
	tree = ET.parse(queryFile)
	root = tree.getroot()
	queryDict = {}
	for tag in root.iter('topic'):
		queryText = ''
		for child in tag:
			if child.tag == 'number':
				queryID = child.text[14:]
			else:	
				text = child.text
				text = preproVocab.removeSymbols(text)
				queryText += text
		queryDict[queryID] = queryText
	return queryDict

def pruneQuery_PTV(queryFile):
	with open(queryFile) as file:
		content = file.read()
	lines = content.splitlines()
	queryDict = {idx + 1 : preproVocab.removeSymbols(lines[idx]) for idx in range(len(lines))}
	return queryDict



def query_syllable(entryType, queryDict, wordToIdxVocab, inverted, termDocSize):
	row_doc = []
	col_term = []
	score = []
	CNS_ZhuYin = preproVocab.buildMapping()
	for query_idx, queryID in enumerate(queryDict):
		query_count = {}
		text = queryDict[queryID]
		lens = len(text) if entryType == 'char' else len(text) - 1
		for i in range(lens):
			if entryType == 'mono_syllable':
				if text[i] in CNS_ZhuYin:
					for ZhuYin in CNS_ZhuYin[text[i]]:
						if ZhuYin in wordToIdxVocab:
							if wordToIdxVocab[ZhuYin] in query_count:
								query_count[wordToIdxVocab[ZhuYin]] += 1
							else:
								query_count[wordToIdxVocab[ZhuYin]] = 1
			else:
				if text[i] in CNS_ZhuYin and text[i + 1] in CNS_ZhuYin:
					for z1 in CNS_ZhuYin[text[i]]:
						for z2 in CNS_ZhuYin[text[i + 1]]:
							entry = z1 + z2
							if entry in wordToIdxVocab:
								if wordToIdxVocab[entry] in query_count:
									query_count[wordToIdxVocab[entry]] += 1
								else:
									query_count[wordToIdxVocab[entry]] = 1
		for term_idx in query_count:
			tf = query_count[term_idx]
			idf = float(termDocSize[1]) / len(inverted[term_idx])
			tfidf = tf * idf
			row_doc.append(query_idx)
			col_term.append(term_idx)
			score.append(tfidf)
	query_matrix = csr_matrix((score, (row_doc, col_term)), shape=(len(queryDict), termDocSize[0]))
	return query_matrix

def query_char(entryType, queryDict, wordToIdxVocab, inverted, termDocSize):
	row_doc = []
	col_term = []
	score = []
	for query_idx, queryID in enumerate(queryDict):
		query_count = {}
		text = queryDict[queryID]
		lens = len(text) if entryType == 'char' else len(text) - 1
		for i in range(lens):
			entry = text[i] if entryType == 'char' else text[i] + text[i + 1]
			if entry in wordToIdxVocab:
				if wordToIdxVocab[entry] in query_count:
					query_count[wordToIdxVocab[entry]] += 1
				else:
					query_count[wordToIdxVocab[entry]] = 1
	
		for term_idx in query_count:
			tf = query_count[term_idx]
			idf = float(termDocSize[1]) / len(inverted[term_idx])
			tfidf = tf * idf
			row_doc.append(query_idx)
			col_term.append(term_idx)
			score.append(tfidf)
	query_matrix = csr_matrix((score, (row_doc, col_term)), shape=(len(queryDict), termDocSize[0]))
	return query_matrix
def query_word(queryDict, wordToIdxVocab, inverted, termDocSize):
	row_doc = []
	col_term = []
	score = []
	for query_idx, queryID in enumerate(queryDict):
		query_count = {}
		tmp = jieba.cut(queryDict[queryID])
		words = ' '.join(tmp).split()
		for word in words:
			if word in wordToIdxVocab:
				if wordToIdxVocab[word] in query_count:
					query_count[wordToIdxVocab[word]] += 1
				else:
					query_count[wordToIdxVocab[word]] = 1
		for term_idx in query_count:
			tf = query_count[term_idx]
			idf = float(termDocSize[1]) / len(inverted[term_idx])
			tfidf = tf * idf
			row_doc.append(query_idx)
			col_term.append(term_idx)
			score.append(tfidf)
	query_matrix = csr_matrix((score, (row_doc, col_term)), shape=(len(queryDict), termDocSize[0]))
	return query_matrix

def predict(model_matrix, query_matrix):
	doc_scores = query_matrix.dot(model_matrix)
	doc_scores = doc_scores.todense()
	doc_rank = np.argsort(doc_scores, axis=1)[:, :5]
	return doc_rank

def buildDocMatrix(inverted, termDocSize, corpus_dict, IdxToDoc, averageDocLen):
	row_term = []
	col_doc = []
	score = []
	for term_idx in inverted:
		doc_num = len(inverted[term_idx])
		idf =  math.log(float(termDocSize[1]) / len(inverted[term_idx]))
		for doc_idx in inverted[term_idx]:
			tf = (BM_K + 1) * inverted[term_idx][doc_idx] / (inverted[term_idx][doc_idx] + BM_K * (1 - BM_B + BM_B * corpus_dict[IdxToDoc[doc_idx]][0] / averageDocLen))     
			tfidf = tf * idf
			row_term.append(term_idx)
			col_doc.append(doc_idx)
			score.append(tfidf)
	model_matrix = csr_matrix((score, (row_term, col_doc)), shape=(termDocSize[0], termDocSize[1]))
	return model_matrix

def buildQueryMatrix(entryType, dataType, wordToIdxVocabFile, invertedFile, termDocSizeFile):
	wordToIdxVocab = preproVocab.readPickle(wordToIdxVocabFile)
	inverted = preproVocab.readPickle(invertedFile)
	termDocSize = preproVocab.readPickle(termDocSizeFile)

	queryFile = os.path.join(queryPath, dataType + '.query')
	if dataType == 'CIRB':
		queryDict = pruneQuery_CIRB(queryFile)
	elif dataType == 'PTV':
		queryDict = pruneQuery_PTV(queryFile)
	else:
		print('wrong dataType in buildQueryMatrix')
		return

	if entryType == 'mono_syllable':
		query_matrix = query_syllable(entryType, queryDict, wordToIdxVocab, inverted, termDocSize)
	elif entryType == 'bi_syllable':
		query_matrix = query_syllable(entryType, queryDict, wordToIdxVocab, inverted, termDocSize)
	elif entryType == 'char':
		query_matrix = query_char(entryType, queryDict, wordToIdxVocab, inverted, termDocSize)
	elif entryType == 'bi_char':
		query_matrix = query_char(entryType, queryDict, wordToIdxVocab, inverted, termDocSize)
	elif entryType == 'word':
		query_matrix = query_word(queryDict, wordToIdxVocab, inverted, termDocSize)
	else:
		print('wrong entryType in buildQueryMatrix')
		return
	return query_matrix

def main(entryType, corpusDictFile, invertedFile, termDocSizeFile, IdxToDocFile, averageDocLenFile):
	corpus_dict = preproVocab.readPickle(corpusDictFile)
	inverted = preproVocab.readPickle(invertedFile)
	termDocSize = preproVocab.readPickle(termDocSizeFile)
	IdxToDoc = preproVocab.readPickle(IdxToDocFile)
	averageDocLen = preproVocab.readPickle(averageDocLenFile)
	model_matrix = buildDocMatrix(inverted, termDocSize, corpus_dict, IdxToDoc, averageDocLen)
	
	# print(model_matrix.shape)
	preproVocab.writePickle(model_matrix, os.path.join(dataPath, 'model'))
	return model_matrix



if __name__ == '__main__':
	entryType = 'char'
	corpusDictFile = os.path.join(dataPath, 'PTV_corpusDict.pkl')
	invertedFile = os.path.join(dataPath, 'PTV_char_inverted.pkl')
	IdxToDocFile = os.path.join(dataPath, 'PTV_IdxToDoc.pkl')
	termDocSizeFile = os.path.join(dataPath, 'PTV_char_termDocSize.pkl')
	averageDocLenFile = os.path.join(dataPath, 'PTV_averageDocLen.pkl')

	main(entryType, corpusDictFile, invertedFile, termDocSizeFile, IdxToDocFile, averageDocLenFile)
