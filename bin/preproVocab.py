import numpy as np
import chardet
import xml.etree.ElementTree as ET
import jieba
import pickle
import os 
import re

### depend on dataset
RARE_BI_S_THRE = 5
RARE_CHAR_THRE = 5
STOP_WORD_THRE = 10000
STOP_CHAR_THRE = 50000
STOP_MONO_THRE = 1000000

tones = [711, 714, 715, 717, 729]
symbols = [10, 32, 58, 12289, 12290, 12291, 12293, 12300, 12301, 65281, 65288, 65289, 65292, 65294, 65307, 65343] 

mappingFilename = 'Big5-ZhuYin.map'
pinyinFilename = 'CNS_pinyin.txt'
rootPath = '..'
dataPath = os.path.join(rootPath, 'data')

def writePickle(data, filename):
	with open(filename, 'wb') as file:
		pickle.dump(data, file)
	return
def readPickle(filename):
	with open(filename, 'rb') as file:
		data = pickle.load(file)
	return data

def transformFormat_CIRB(corpusPath, outputFile, averageDocLenFile):
	fileListFile = os.path.join(rootPath, 'file-list')
	with open(fileListFile) as file:
		content = file.read()
	fileList = content.splitlines()

	file_dict = {}
	ave_doc_len = 0
	for path in fileList:
		filename = os.path.join(corpusPath, path)
		DocLen = 0
		Doc = ''
		tree = ET.parse(filename)
		root = tree.getroot()
		for tag in root.iter('p'):
			text = tag.text
			text = removeSymbols(text)
			DocLen += len(text)
			Doc += text
		file_dict[path.lower()] = [DocLen, Doc]
		ave_doc_len += DocLen
	writePickle(float(ave_doc_len) / len(file_dict), averageDocLenFile)
	writePickle(file_dict, outputFile)
	return 

def transformFormat_PTV(corpusPath, outputFile, averageDocLenFile):
	dirs = os.path.join(corpusPath, 'transcripts')
	file_dict = {}
	ave_doc_len = 0
	for path in os.listdirs(dirs):
		filename = os.path.join(dirs, path)
		with open(filename) as file:
			content = file.read()
		content = removeSymbols(content)
		file_dict[path] = [len(content), content]
		ave_doc_len += len(content)
	writePickle(float(ave_doc_len) / len(file_dict), averageDocLenFile)
	writePickle(file_dict, outputFile)
	return

def buildMapping():
	CNS_ZhuYin = {}
	filename = os.path.join(rootPath, mappingFilename)
	with open(filename) as file:
		content = file.read()
	mappings = content.splitlines()
	for mapping in mappings:
		tmp = mapping.split()
		word = tmp[0]
		ZhuYins = [removeTones(ZhuYin) for ZhuYin in tmp[1].split('/')]
		CNS_ZhuYin[word] = ZhuYins
	return CNS_ZhuYin

def removeSymbols(text):
	for symbol in symbols:
		text = text.replace(chr(symbol), '')
	return text
def removeTones(ZhuYins):
	for tone in tones:
		ZhuYins = ZhuYins.replace(chr(tone), '')
	return ZhuYins

def pruneVocabDict(CNS_wordToIdx, CNS_idxToCount, wordToIdxVocabFile, IdxToCountVocabFile):
	# new_count = CNS_idxToCount
	new_dict = {}
	for word in CNS_wordToIdx:
		if CNS_idxToCount[ CNS_wordToIdx[word] ] != 0:
			new_dict.setdefault(word, len(new_dict))
	new_count = np.array([count for count in CNS_idxToCount if count != 0])	
	
	writePickle(new_dict, wordToIdxVocabFile)
	writePickle(new_count, IdxToCountVocabFile)
	return

def buildVocabDictandCount_char(corpusDictFile, entryType):
	corpus_dict = readPickle(corpusDictFile)
	
	CNS_Dict = {}
	CNS_tmp = {}
	for doc in corpus_dict:
		text = removeSymbols(corpus_dict[doc][1])
		lens = len(text) if entryType == 'char' else len(text) - 1
		for i in range(lens):
			entry = text[i] if entryType == 'char' else text[i] + text[i + 1]
			CNS_Dict.setdefault(entry, len(CNS_Dict))
			if entry in CNS_tmp:
				CNS_tmp[entry] += 1
			else:
				CNS_tmp[entry] = 1
	CNS_Count = np.zeros(len(CNS_Dict))
	for entry in CNS_Dict:
		if CNS_tmp[entry] < STOP_CHAR_THRE and CNS_tmp[entry] > RARE_CHAR_THRE:
			CNS_Count[CNS_Dict[entry]] = CNS_tmp[entry]
		else:
			CNS_Count[CNS_Dict[entry]] = 0
	return CNS_Dict, CNS_Count

def buildVocabCount_mono(Dic, corpusDictFile):

	CNS_ZhuYin = buildMapping()
	CNS_idxToCount = np.zeros(len(Dic))
	corpus_dict = readPickle(corpusDictFile)
	
	for doc in corpus_dict:
		text = removeSymbols(corpus_dict[doc][1])
		for word in text:
			if word in CNS_ZhuYin:
				for ZhuYin in CNS_ZhuYin[word]:
					if ZhuYin in Dic:
						CNS_idxToCount[Dic[ZhuYin]] += 1
	CNS_idxToCount = np.array([i if i < STOP_MONO_THRE else 0 for i in CNS_idxToCount])
	return CNS_idxToCount

def buildVocabDict_mono():
	filename = os.path.join(rootPath, pinyinFilename)
	with open(filename, encoding = 'utf-16') as file:
		content = file.read()
	lines = content.splitlines()
	vocab_dict = {}
	for line in lines:
		ZhuYins = removeTones(line.split()[0])
		vocab_dict.setdefault(ZhuYins, len(vocab_dict))
	return vocab_dict

def buildVocabCount_bi(Dic, corpusDictFile):
	CNS_ZhuYin = buildMapping()
	CNS_idxToCount = np.zeros(len(Dic))
	corpus_dict = readPickle(corpusDictFile)
	
	for doc in corpus_dict:
		text = removeSymbols(corpus_dict[doc][1])
		for i in range(len(text) - 1):
			if text[i] in CNS_ZhuYin and text[i + 1] in CNS_ZhuYin:
				for z1 in CNS_ZhuYin[text[i]]:
					for z2 in CNS_ZhuYin[text[i + 1]]:
						if z1 + z2 in Dic:
							CNS_idxToCount[Dic[z1 + z2]] += 1
	CNS_idxToCount = np.array([i if i > RARE_BI_S_THRE else 0 for i in CNS_idxToCount])
	return CNS_idxToCount

def buildVocabDict_bi(CNS_wordToIdx):
	new_dict = {}
	for w1 in CNS_wordToIdx:
		for w2 in CNS_wordToIdx:
			new_dict.setdefault(w1 + w2, len(new_dict))
	return new_dict

def buildVocabDictandCount_word(corpusDictFile):
	corpus_dict = readPickle(corpusDictFile)
	
	word_dict = {}
	count_dict = {}
	for doc in corpus_dict:
		tmp = jieba.cut(corpus_dict[doc][1])
		words = ' '.join(tmp).split()
		for word in words:
			word = removeSymbols(word)
			word_dict.setdefault(word, len(word_dict))
			if word in count_dict:
				count_dict[word] += 1
			else:
				count_dict[word] = 1
	CNS_idxToCount = np.zeros(len(word_dict))
	for word in word_dict:
		if count_dict[word] > 1 and count_dict[word] < STOP_WORD_THRE:
			CNS_idxToCount[word_dict[word]] = count_dict[word]
		else:
			CNS_idxToCount[word_dict[word]] = 0
	return word_dict, CNS_idxToCount

def main(corpusDictFile, entryType, wordToIdxVocabFile, IdxToCountVocabFile):
	### different from 4 types ##############################################################
	
	if entryType == 'mono_syllable':
		CNS_wordToIdx = buildVocabDict_mono()
		CNS_idxToCount = buildVocabCount_mono(CNS_wordToIdx, corpusDictFile)
	elif entryType == 'bi_syllable':
		CNS_wordToIdx = buildVocabDict_mono()
		CNS_wordToIdx = buildVocabDict_bi(CNS_wordToIdx)
		CNS_idxToCount = buildVocabCount_bi(CNS_wordToIdx, corpusDictFile)
	elif entryType == 'char':
		CNS_wordToIdx, CNS_idxToCount = buildVocabDictandCount_char(corpusDictFile, entryType) 												# dict  						  								
	elif entryType == 'bi_char':
		CNS_wordToIdx, CNS_idxToCount = buildVocabDictandCount_char(corpusDictFile, entryType)
	elif entryType == 'word':
		CNS_wordToIdx, CNS_idxToCount = buildVocabDictandCount_word(corpusDictFile)
	else:
		print('wrong entryType')
		return 
	pruneVocabDict(CNS_wordToIdx, CNS_idxToCount, wordToIdxVocabFile, IdxToCountVocabFile)											# dict, np array	
	return
#############################################################################################

if __name__ == '__main__':
	entryType = 'char'
	corpusDictFile = os.path.join(dataPath, 'PTV_corpusDict.pkl')
	wordToIdxVocabFile = os.path.join(dataPath, 'PTV_word_WordToIdxVocabDict.pkl')
	IdxToCountVocabFile = os.path.join(dataPath, 'PTV_word_IdxToCountVocab.pkl')
	main(corpusDictFile, entryType, wordToIdxVocabFile, IdxToCountVocabFile)